package main
/*
lab 1.
Create a thread-like slice type with a name
safeSlice, which implements the following exported inter-
SafeSlice
*/
type safeSlice chan commandData

type commandData struct {
	action  commandAction
	index   int
	item    interface{}
	result  chan<- interface{}
	data    chan<- []interface{}
	updater UpdateFunc
}

type commandAction int

//the list of commands
const (
	insert commandAction = iota
	remove
	at
	update
	end
	length
)

//Update
type UpdateFunc func(interface{}) interface{}

type SafeSlice interface {
	Append(interface{})     // Append item to slice
	At(int) interface{}     // Return the item at index position
	Close() []interface{}   // Close the channel
	Delete(int)             // Delete the item at  index position
	Len() int               // Return the number of items in the slice
	Update(int, UpdateFunc) // Update the item at the given index position
}

func New() SafeSlice {
	// The make built-in function allocates and initializes an object of type
	// slice, map, or chan (only)
	slice := make(safeSlice)
	go slice.run()
	return slice
}

func (slice safeSlice) run() {
	list := make([]interface{}, 0)
	for command := range slice {
		switch command.action {
		// to append item to the list
		case insert:
			list = append(list, command.item)
			// to delete item from list
		case remove:
			if 0 <= command.index && command.index < len(list) {
				list = append(list[:command.index],
					list[command.index+1:]...)
			}
			//to get the item at position
		case at:
			if 0 <= command.index && command.index < len(list) {
				command.result <- list[command.index]
			} else {
				command.result <- nil
			}
			// to get thi ssize
		case length:
			command.result <- len(list)
			//to update the item with given index
		case update:
			if 0 <= command.index && command.index < len(list) {
				list[command.index] = command.updater(list[command.index])
			}
			// to close the channel and return slice
		case end:
			close(slice)
			command.data <- list
		}
	}
}

func (slice safeSlice) Append(item interface{}) {
	slice <- commandData{action: insert, item: item}
}

func (slice safeSlice) Delete(index int) {
	slice <- commandData{action: remove, index: index}
}

func (slice safeSlice) At(index int) interface{} {
	reply := make(chan interface{})
	slice <- commandData{at, index, nil, reply, nil, nil}
	return <-reply
}

func (slice safeSlice) Len() int {
	reply := make(chan interface{})
	slice <- commandData{action: length, result: reply}
	return (<-reply).(int)
}

func (slice safeSlice) Update(index int, updater UpdateFunc) {
	slice <- commandData{action: update, index: index, updater: updater}
}

func (slice safeSlice) Close() []interface{} {
	reply := make(chan []interface{})
	slice <- commandData{action: end, data: reply}
	return <-reply
}
